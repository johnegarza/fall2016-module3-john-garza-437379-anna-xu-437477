<!DOCTYPE html>
    <html>
        <head>
            <title>
                Submit Story
            </title>
        </head>
        <body>
            <?php session_start(); ?>
            
            <form method='POST' action='storyHandler.php'>
                <p>Title: <input type='text' name='title'></p>
                <p>Link: <input type='url' name='externalLink'></p>
                Story:<br>
                <textarea name="body" cols=75 rows=10></textarea>
                <br>
                <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>">
                <button type='submit'>Submit</button>
            </form>
        </body>
    </html>
