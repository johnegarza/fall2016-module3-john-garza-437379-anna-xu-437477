<!DOCTYPE html>

<html>
    <head>
        <title>Login</title>
    </head>
    <body>
        <?php
            //to clear any previous sessions, unset user and token
            session_start();
            unset($_SESSION['username']);
            unset($_SESSION['token']);
        ?>

        <!-- create login boxes -->
        <form method="post" action="loginHandler.php">
            Username:<input type="text" name="username">
            Password:<input type="password" name="password">
            <button type='submit'>Login</button>
        </form>

        <!-- create new user -->
        <form action='createNewUser.html'>
            <button type='submit'>Create New User</button>
        </form>

        <!-- back/homepage button -->
        <form action='wureddit.php'>
            <button type='submit'>Back to wureddit homepage</button>
        </form>

    </body>
</html>
