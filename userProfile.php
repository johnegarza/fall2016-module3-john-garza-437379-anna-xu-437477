<!DOCTYPE html>

<html>
    <head>
        <title> Profile </title>
    </head>
    <body>

<?php
	require 'mysqlConnect.php';
	session_start();

	//Create back to main page button
	echo "<form action='wureddit.php'>
        <button type='submit'>Back to wureddit homepage</button>
        </form>";

	//show all files associated with a user
	$username = $_SESSION['username'];
	$stmt = $mysqli->prepare("SELECT title, internal_link FROM stories WHERE author='$username'");
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	$stmt->execute();
	$stmt->bind_result($title, $internalLink);
	echo "$username's stories <br><br>";
	while($stmt->fetch()){
		echo "<a href='$internalLink'>
			$title
		        </a><br>";
	}
	echo "<br><form action='emailVerification.php' method='POST'>
		Verify your email: <input type='text' name='email'>
		<input type='hidden' name='token' value={$_SESSION['token']}>
        	<button type='submit'>Go</button>
        	</form>";
?>
    </body>
</html>
