<?php
    require 'mysqlConnect.php';
    
    $stmt = $mysqli->prepare('INSERT INTO users (username, password) VALUES (?, ?)');
    $user = $_POST['newUser'];

    if( !preg_match(' /^[\w_\-]+$/', $user) ){
        echo "Invalid username";
        echo "<br>";
        echo "Redirecting in 5 seconds...";
        header("Refresh: 5; URL=http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/createNewUser.html");
        exit;
    }
    if( !preg_match(' /^[\w_\-]+$/', $_POST['newPassword']) ){
        echo "Invalid password";
        echo "<br>";
        echo "Redirecting in 5 seconds...";
        header("Refresh: 5; URL=http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/createNewUser.html");
        exit;
    }
    if($_POST['newPassword']==$_POST['confirmPassword']){
        $encryptedPassword = crypt($_POST['newPassword']);
        $stmt->bind_param('ss', $user, $encryptedPassword);
        $stmt->execute();
        header ( 'Location: wuredditLogin.php' );
    }
    else{
        echo "Passwords do not match";
        echo "<br>";
        echo "Redirecting in 5 seconds...";
        header("Refresh: 5; URL=http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/createNewUser.html");
    }
?>
