<?php
    //Script to edit comment in batabase
    require 'mysqlConnect.php';
    session_start();
    
    $newComment=$_POST['newComment'];
    $title = $_POST['title'];
    $unspaced = str_replace(' ', '%20', $title);
    $redirectPage = "http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/viewer.php?title=".$unspaced;

    if( preg_match('/[^0-9a-z\s-\?\!\;\:\_\.]/i', $newComment) ){
        echo "Comment cannot contain special characters";
        echo "<br>";
        echo "Redirecting in 5 seconds...";
        header("Refresh: 5; URL=$redirectPage");
        exit;
    }

    $id=$_POST['comment_id'];
    
    if($_SESSION['token'] !== $_POST['token']){
        die("Request forgery detected");
    }
    
    $stmt=$mysqli->prepare('UPDATE comments SET body=? WHERE id=?');
    if(!$stmt){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
    $stmt->bind_param('si', $newComment, $id);
    $stmt->execute();
    
    header( "Location: $redirectPage");
?>
