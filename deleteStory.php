<?php
    //Script for deleting story and all references to  that story from database
    require 'mysqlConnect.php';
    session_start();
    
    $title=$_POST['story'];
    
    if($_SESSION['token'] !== $_POST['token']){
        die("Request forgery detected");
    }
    
    //Delete comments on story
    $stmt = $mysqli->prepare('DELETE FROM comments WHERE story_title=?');
    if(!$stmt){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
    $stmt->bind_param('s', $title);
    $stmt->execute();
    
    //Delete story
    $stmt2 = $mysqli->prepare('DELETE FROM stories WHERE title=?');
    if(!$stmt2){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
    $stmt2->bind_param('s', $title);
    $stmt2->execute();
    
    header( 'Location: wureddit.php');
?>
