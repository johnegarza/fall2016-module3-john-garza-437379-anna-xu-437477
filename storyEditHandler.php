<?php
    //Script for submitting editted story to database
    require 'mysqlConnect.php';
    session_start();
    
    $originalTitle=$_POST['title'];
    $newTitle=$_POST['newTitle'];
    $unspaced = str_replace(' ', '%20', $originalTitle);
    $redirectPage = "http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/viewer.php?title=".$unspaced;


    if( preg_match('/[^0-9a-z\s-\?\!\;\:\_\.]/i', $newTitle) ){
        echo "Title cannot contain special characters";
        echo "<br>";
        echo "Redirecting in 5 seconds...";
        header("Refresh: 5; URL=$redirectPage");
        exit;
    }

    $newBody=$_POST['newStory'];
    if( preg_match('/[^0-9a-z\s-\?\!\;\:\_\.]/i', $newBody) ){
        echo "Story cannot contain special characters";
        echo "<br>";
        echo "Redirecting in 5 seconds...";
        header("Refresh: 5; URL=$redirectPage");
        exit;
    }
    $newExternalLink = $_POST['newLink'];    
    $newExternalLink = filter_var($newExternalLink, FILTER_SANITIZE_URL);
    if (!filter_var($newExternalLink, FILTER_VALIDATE_URL) === false) {
    }
    else {
        echo("Not a valid URL");
        echo "<br>";
        echo "Redirecting in 5 seconds...";
        header("Refresh: 5; URL=$redirectPage");
        exit;
    }

    if($_SESSION['token'] !== $_POST['token']){
        die("Request forgery detected");
    }
    
    $newUnspaced = str_replace(' ', '%20', $newTitle);
    $newRedirectPage = "http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/viewer.php?title=".$newUnspaced;


    $stmt=$mysqli->prepare('UPDATE stories SET title=?, body=?, internal_link=?, external_link=? WHERE title=?');
    if(!$stmt){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
    $stmt->bind_param('sssss', $newTitle, $newBody, $newRedirectPage, $newExternalLink, $originalTitle);
    $stmt->execute();
    
    header ( "Location: $newRedirectPage");
?>
