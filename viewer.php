<!DOCTYPE html>
    <html>
        <head>
            <title>wureddit stories</title>
        </head>
        <body>
            <?php
                require 'mysqlConnect.php';
                session_start();
                
                //Create back to main page button
                echo "<form action='wureddit.php'>
                            <button type='submit'>Back to wureddit homepage</button>
                      </form>";
                      
                
                //Retrieve story
                $title = $_GET['title'];
                $stmt = $mysqli->prepare('SELECT author, external_link, body FROM stories WHERE title=?');
                $stmt->bind_param('s', $title);
                $stmt->execute();
                $stmt->bind_result($author, $externalLink, $body);

                //Display that stories title and body and applicable buttons depending on user
                while($stmt->fetch()){
                    echo htmlentities($title). " by " .htmlentities($author). "<br>";
                    echo "<a href='$externalLink'>Link</a> <br>";
                    echo htmlentities($body). "<br> ";

                    //If logged into owner of story, display edit and delete buttons
                    if (isset($_SESSION['username'])){
                        if ( $_SESSION['username']==$author ){
                            echo "<form action='editStory.php' method='POST'>
                                    <input type='hidden' name='title' value='$title'>
                                    <input type='hidden' name='token' value={$_SESSION['token']}>
                                    <button type='submit'>Edit Story</button>
                                  </form>";
                            echo "<form action='deleteStory.php' method='POST'>
                                    <input type='hidden' name='story' value='$title'>
                                    <input type='hidden' name='token' value={$_SESSION['token']}>
                                    <button type='submit'>Delete Story</button> <br> <br>
                                  </form>";
                        }
                    }
                }


                //Display add comment button
                if(isset($_SESSION['username'])){

		echo "<form action='save.php' method='POST'>
                       	<input type='hidden' name='title' value='$title'>
                       	<input type='hidden' name='token' value={$_SESSION['token']}>
                       	<button type='submit'>Save Story</button> <br> <br>
                       	</form>";


                    echo "<form action='submitComment.php' method='POST'>
                                <p>Comment: </p>
                                <textarea name='newComment' rows=4 cols=75></textarea>
                                <input type='hidden' name='story' value='$title'>
                                <input type='hidden' name='token' value={$_SESSION['token']}> <br>
                                <button type='submit'>Submit Comment</button>
                          </form>";
                }

                //Retrieves comments on story from database
                $stmt2 = $mysqli->prepare('SELECT body, author, id FROM comments WHERE story_title=?');
                if(!$stmt2){
                    printf("Query Prep Failed: %s\n", $mysqli->error);
                    exit;
                }
                $stmt2->bind_param('s', $title);
                $stmt2->execute();
                $result=$stmt2->get_result();
                $stmt2->close();

                //Display those comments/buttons
                while($row = $result->fetch_assoc()){
                    $comment = $row['body'];
                    $commentAuthor = $row['author'];
                    $id = $row['id'];
                    echo "<br>".htmlentities($comment)."<br>";
                    echo "-";
                    echo htmlentities($commentAuthor);

                    //If logged into owner of comment, displays delete, edit buttons, like, and unlike buttons
                    if(isset($_SESSION['username']) && isset($commentAuthor)){
                        if($_SESSION['username']==$commentAuthor){
                            //edit button
                            echo "<form action='editComment.php' method='POST'>
                                        <input type='hidden' name='comment_id' value='$id'>
                                        <input type='hidden' name='title' value='$title'>
                                        <input type='hidden' name='token' value={$_SESSION['token']}>
                                        <button type='submit'>Edit</button>
                                  </form>";
                            //delete button
                            echo "<form action='deleteComment.php' method='POST'>
                                        <input type='hidden' name='comment_id' value='$id'>
                                        <input type='hidden' name='title' value='$title'>
                                        <input type='hidden' name='token' value={$_SESSION['token']}>
                                        <button type='submit'>Delete</button>
                                  </form>";
                        }
                    }
                    echo "<br>";
                }
            ?>
        </body>
    </html>
