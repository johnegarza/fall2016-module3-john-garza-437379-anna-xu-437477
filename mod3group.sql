-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 03, 2017 at 11:59 PM
-- Server version: 5.5.53-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `groupProject`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `body` text NOT NULL,
  `author` varchar(255) NOT NULL,
  `story_title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `author` (`author`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `body`, `author`, `story_title`) VALUES
(1, 'cant wait for the debate!', 'tester', 'Trumpity Bumpity'),
(2, 'uh oh spaghetti 0 :o', 'tester', 'kimmy'),
(3, 'wow!!!', 'annaxu', 'Another tech support story'),
(4, 'wow???', 'annaxu', 'Another tech support story'),
(6, 'hi', 'ab', 'kimmy');

-- --------------------------------------------------------

--
-- Table structure for table `saved`
--

CREATE TABLE IF NOT EXISTS `saved` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `internal_link` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `saved`
--

INSERT INTO `saved` (`id`, `username`, `title`, `internal_link`) VALUES
(7, 'annaxu', 'Tech support story 1', 'http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/viewer.php?title=Tech%20support%20story%201'),
(8, 'tester', 'Trumpity Bumpity', 'http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/viewer.php?title=Trumpity%20Bumpity'),
(9, 'tester', 'kimmy', 'http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/viewer.php?title=kimmy'),
(10, 'ab', 'kimmy', 'http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/viewer.php?title=kimmy'),
(11, 'tester', 'Last example', 'http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/viewer.php?title=Last%20example'),
(12, 'brian', 'Another tech support story', 'http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/viewer.php?title=Another%20tech%20support%20story');

-- --------------------------------------------------------

--
-- Table structure for table `stories`
--

CREATE TABLE IF NOT EXISTS `stories` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `author` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `internal_link` varchar(255) NOT NULL,
  `external_link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `author` (`author`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `stories`
--

INSERT INTO `stories` (`id`, `author`, `title`, `body`, `internal_link`, `external_link`) VALUES
(2, 'annaxu', 'Trumpity Bumpity', 'Scope this dank page', 'http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/viewer.php?title=Trumpity%20Bumpity', 'http://www.nytimes.com/2016/10/03/business/how-donald-trump-turned-the-tax-code-into-a-giant-tax-shelter.html'),
(3, 'tester', 'Another tech support story', 'Also from reddit', 'http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/viewer.php?title=Another%20tech%20support%20story', 'https://www.reddit.com/r/talesfromtechsupport/comments/55mww9/user_cant_find_the_files_he_asked_to_have_moved/'),
(4, 'tester', 'Last example', 'this is an example of the story description blah blah blah blahhhhhhhhhhhh', 'http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/viewer.php?title=Last%20example', 'https://www.reddit.com/r/talesfromtechsupport/comments/55kr64/everything_is_it/'),
(5, 'annaxu', 'kimmy', 'bb got robbed!!', 'http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/viewer.php?title=kimmy', 'https://www.buzzfeed.com/claudiakoerner/kim-kardashian-was-held-at-gunpoint-in-her-paris-hotel-room?utm_term=.aukgmBYE6#.wheoNk8b0');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`username`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `password`) VALUES
('ab', '$1$FpRPYdCC$MwfwX.tkWpIo5XEGCIEQm.'),
('annaxu', '$1$9nZGMmUO$FCCA94TrGOxhierRVcv7I.'),
('brian', '$1$ArMAylGh$ILYnQMkeO5tAGA55kvkIV1'),
('tester', '$1$Z/6hi25u$G/8PIO.bHAzffH0AEWBHN1');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`author`) REFERENCES `users` (`username`);

--
-- Constraints for table `saved`
--
ALTER TABLE `saved`
  ADD CONSTRAINT `saved_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`);

--
-- Constraints for table `stories`
--
ALTER TABLE `stories`
  ADD CONSTRAINT `stories_ibfk_1` FOREIGN KEY (`author`) REFERENCES `users` (`username`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
