<?php

    require 'mysqlConnect.php';
    session_start();
    
    $username = $_SESSION['username'];
    $newComment = $_POST['newComment'];
    $story = $_POST['story'];
    $unspaced = str_replace(' ', '%20', $story);
    $redirectPage = "http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/viewer.php?title=".$unspaced;


    if( preg_match('/[^0-9a-z\s-\?\!\;\:\_\.]/i', $newComment) ){
        echo "Comment cannot contain special characters";
        echo "<br>";
        echo "Redirecting in 5 seconds...";
        header("Refresh: 5; URL=$redirectPage");
        exit;
    }
    
    if($_SESSION['token'] !== $_POST['token']){
        die("Request forgery detected");
    }
    
    $stmt = $mysqli->prepare('INSERT INTO comments (body, author, story_title) VALUES (?, ?, ?)');
    if(!$stmt){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
    $stmt->bind_param('sss', $newComment, $username, $story);
    $stmt->execute();
    
    header( "Location: $redirectPage");
    
?>
