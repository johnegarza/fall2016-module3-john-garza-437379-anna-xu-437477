<?php
   
    require 'mysqlConnect.php';

    $username = $_POST['username'];
    if( !preg_match(' /^[\w_\-]+$/', $username) ){
        echo "Invalid username";
        echo "<br>";
        echo "Redirecting in 5 seconds...";
        header("Refresh: 5; URL=http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/wuredditLogin.php");
        exit;
    }
    $password = $_POST['password'];
    if( !preg_match(' /^[\w_\-]+$/', $password) ){
        echo "Invalid password";
        echo "<br>";
        echo "Redirecting in 5 seconds...";
        header("Refresh: 5; URL=http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/wuredditLogin.php");
        exit;
    }
    
    //Retrieve users and encrypted pws from database
    $stmt = $mysqli->prepare("SELECT username, password FROM users");
    if(!$stmt){
        printf("Query Prep Failed: %s\n", $mtsqli->error);
        exit;
    }
    $stmt->execute();
    $stmt->bind_result($dbUsername, $dbPassword);
    
    //Check each user/pw pair with input user and recently encrypted/salted pw
    while($stmt->fetch()){

        //Login if match is found and create CSRF token
        if($username==$dbUsername && crypt($password, $dbPassword)==$dbPassword){
            session_start();
            $_SESSION['username'] = $username;
            $token = substr(md5(rand()), 0, 10);
            $_SESSION['token'] = $token;
            header( 'Location: wureddit.php');
            exit;
        }
    }
    //No match found, redirect to invalid login page
    header( 'Location: invalidLogin.php' );
?>
