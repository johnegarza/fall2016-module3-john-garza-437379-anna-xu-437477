<?php
	require 'mysqlConnect.php';
        session_start();
	$title = $_POST['title'];
	$username = $_SESSION['username'];
	$stmt = $mysqli->prepare('INSERT INTO saved (username, title, internal_link) VALUES (?, ?, ?)');
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}

	$internalLink = 'http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/viewer.php?title=' . 				$title;
	$internalLink = str_replace(' ', '%20', $internalLink);

	$stmt->bind_param('sss', $username, $title, $internalLink);
	$stmt->execute();
	header ( "Location: $internalLink");
?>

