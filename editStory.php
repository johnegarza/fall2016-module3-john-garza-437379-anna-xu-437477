<!DOCTYPE html>
    <html>
        <head>
            <title>
                Story Editor
            </title>
        </head>
        <body>
            <?php
                require 'mysqlConnect.php';
                session_start();
            
                $title = $_POST['title'];
                $stmt = $mysqli->prepare('SELECT body, external_link FROM stories WHERE title=?');
                if(!$stmt){
                    printf("Query Prep Failed: %s\n", $mysqli->error);
                    exit;
                }
                $stmt->bind_param('s', $title);
                $stmt->execute();
                $stmt->bind_result($body, $externalLink);
                $stmt->fetch();

                echo "<form action='storyEditHandler.php' method='POST'>
                        Title:<textarea name='newTitle'>$title</textarea> <br>
			Link:<textarea name='newLink'>$externalLink</textarea><br>
                        Story:<textarea name='newStory' rows=10 cols=75>$body</textarea><br>
                        <input type='hidden' name='title' value='$title'>
                        <input type='hidden' name='token' value={$_SESSION['token']}>
                        <button type='submit'>Submit</button>
                      </form>";
            ?>
        </body>
    </html>
