<?php

    require 'mysqlConnect.php';
    session_start();

    $body = $_POST['body'];
    $title = $_POST['title'];
    $externalLink = $_POST['externalLink'];
    $username = $_SESSION['username'];


    if(preg_match('/[^0-9a-z\s-\?\!\;\:\_\.]/i', $body) ){
        echo "Body text cannot contain special characters";
        echo "<br>";
        echo "Redirecting in 5 seconds...";
        header("Refresh: 5; URL=http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/submitStory.php");
        exit;
    }
    if( preg_match('/[^0-9a-z\s-\?\!\;\:\_\.]/i', $title) ){
        echo "Title cannot contain special characters";
        echo "<br>";
        echo "Redirecting in 5 seconds...";
        header("Refresh: 5; URL=http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/submitStory.php");
        exit;
    }

        //the following if/else block is from http://www.w3schools.com/php/php_filter.asp
        $externalLink = filter_var($externalLink, FILTER_SANITIZE_URL);
        if (!filter_var($externalLink, FILTER_VALIDATE_URL) === false) {
        }
        else {
            echo("Not a valid URL");
            echo "<br>";
            echo "Redirecting in 5 seconds...";
            header("Refresh: 5; URL=http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/submitStory.php");
            exit;
        }

    if($_SESSION['token'] !== $_POST['token']){
        die("Request forgery detected");
    }

    $stmt = $mysqli->prepare('INSERT INTO stories (author, title, body, internal_link, external_link) VALUES (?, ?, ?, ?, ?)');
    if(!$stmt){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }

    $internalLink = 'http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/viewer.php?title=' . $title;
    $internalLink = str_replace(' ', '%20', $internalLink);

    $stmt->bind_param('sssss', $username, $title, $body, $internalLink, $externalLink);
    $stmt->execute();

    header ( "Location: $internalLink");
?>
