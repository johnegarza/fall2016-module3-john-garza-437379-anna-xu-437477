<?php
    //Script to delete comment from database
    require 'mysqlConnect.php';
    session_start();

    $title = $_POST['title'];
    $unspaced = str_replace(' ', '%20', $title);
    $redirectPage = "http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/viewer.php?title=".$unspaced;
    
    $id = $_POST['comment_id'];
    
    if($_SESSION['token'] !== $_POST['token']){
        die("Request forgery detected");
    }
    
    //Delete comment
    $stmt=$mysqli->prepare('DELETE FROM comments WHERE id=?');
    if(!$stmt){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
    $stmt->bind_param('i', $id);
    $stmt->execute();
    header ( "Location: $redirectPage");
?>
