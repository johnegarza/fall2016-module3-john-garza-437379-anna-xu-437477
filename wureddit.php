<!DOCTYPE html>
       <html>
        <head>
            <title>Welcome to wureddit!</title>
        </head>
        <body>
            <?php
                session_start();
                require 'mysqlConnect.php';

                echo "<p>Stories</p>";
                $stmt = $mysqli->prepare('SELECT author, title, internal_link FROM stories');
                if(!$stmt){
                    printf("Query Prep Failed: %s\n", $mysqli->error);
                    exit;
                }
                $stmt->execute();
                $stmt->bind_result($author, $title, $internalLink);
                while($stmt->fetch()){
                    $description = "submitted by $author <br>";
                    echo "<a href='$internalLink'>
                            $title
                          </a>
                          <br>$description";
                }
                
                //When logged in, displays current user and options to write story and log out
                if(isset($_SESSION['username'])){
                    echo "<br> <form action='submitStory.php'>
                            <button type='submit'>Submit a story</button>
                          </form>";
		    echo "<br><form action = 'userProfile.php'>
			    <button type='submit'> Your stories</button>
			 </form>";
		    echo "<br><form action = 'saveStory.php'>
			    <button type='submit'> Saved stories</button>
			 </form><br>";
                    echo "Logged in as " . htmlentities($_SESSION['username']);
                    echo "<br><br> <form action='wuredditLogin.php'>
                            <button type='submit'>Log Out</button>
                          </form>";
                }


                //If not logged in, display login option
                if(!isset($_SESSION['username'])){
                    echo "<form action='wuredditLogin.php'>
                            <button type='submit'>Log in</button>
                          </form>";
                }
            ?>
        </body>
    </html>
