<?php
	require 'mysqlConnect.php';
	session_start();
	if($_SESSION['token'] !== $_POST['token']){
        	die("Request forgery detected");
	}
	$email = $_POST['email'];

	if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
		if(mail($email,"Email Validated","Sweet! Your email has been registered on WUReddit!")) {
			echo "Email successfully sent<br>";
        		echo "Redirecting in 5 seconds...";
        		header("Refresh: 5; URL=http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/userProfile.php");
		}
		else
			echo "An error occured";
	} 
	else {
  		echo htmlentities($email) ." is not a valid email address<br>";
		echo "Redirecting in 5 seconds...";
        	header("Refresh: 5; URL=http://ec2-54-89-158-12.compute-1.amazonaws.com/~johnegarza/userProfile.php");
	}

?>
