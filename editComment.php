<!DOCTYPE html>
    <html>
        <head>
            <title>
                Comment Editor
            </title>
        </head>
        <body>
            <?php
                require 'mysqlConnect.php';
                session_start();

                $id=$_POST['comment_id'];
		$title = $_POST['title'];
            
                $stmt = $mysqli->prepare('SELECT body FROM comments WHERE id=?');
                if(!$stmt){
                    printf("Query Prep Failed: %s\n", $mysqli->error);
                    exit;
                }
                $stmt->bind_param('i', $id);
                $stmt->execute();
                $stmt->bind_result($body);
                $stmt->fetch();

                //Text boxes for editing comment body
                echo "<form action='commentEditHandler.php' method='POST'>
                        Comment:<textarea name='newComment' rows=4 cols=75>$body</textarea>
                        <input type='hidden' name='comment_id' value='$id'>
                        <input type='hidden' name='title' value='$title'>
                        <input type='hidden' name='token' value={$_SESSION['token']}>
                        <button type='submit'>Submit</button>
                      </form>";
            ?>
        </body>
    </html>
